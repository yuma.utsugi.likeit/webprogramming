﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE user(
    id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean NOT NULL DEFAULT false,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

insert into user(login_id,name,birth_date,password,is_admin,create_date,update_date) 
values('admin','管理者','2000/01/01','password',true,now(),now());
