package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // jspから持ってきたuserIdの値をゲットパラメータで取得
      String id = request.getParameter("id");

      // int型に変換
      int id2 = Integer.valueOf(id);

      // 上記の値をもとにuserDaoのuserDetailメソッドを呼び出す
      UserDao userDao = new UserDao();
      User user = userDao.findById(id2);

      // メソッドの戻り値をリクエストスコープにセット
      request.setAttribute("user", user);

      // フォワード文でuserUpdate.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String password = request.getParameter("password");
      String confirmPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthday = request.getParameter("birth-date");
      String id = request.getParameter("id");

      UserDao userDao = new UserDao();

      // 例外処理
      if (password.equals("") || confirmPassword.equals("") || name.equals("")
          || birthday.equals("") || !password.equals(confirmPassword)) {

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        userDao.userUpdate(password, name, birthday, id);

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");

        // 入力された値を画面に表示するためリクエストスコープに値をセット
        request.setAttribute("name", name);
        request.setAttribute("birthday", birthday);

        // ユーザーに再度入力させるためuserUpdate.jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
    }
  }


