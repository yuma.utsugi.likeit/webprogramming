package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // jspから持ってきたuserIdの値をゲットパラメータで取得
      String id = request.getParameter("id");

      // int型に変換
      int id2 = Integer.valueOf(id);

      // 上記の値をもとにuserDaoのuserDetailメソッドを呼び出す
      UserDao userDao = new UserDao();
      User user = userDao.findById(id2);

      // メソッドの戻り値をリクエストスコープにセット
      request.setAttribute("user", user);

      // フォワード文でuserDelete.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
      return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String id = request.getParameter("id");

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      userDao.userDelete(id);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
	}
}
