package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
      }

      // フォワード文でuserAdd.jspに遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String confirmPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthday = request.getParameter("birth-date");

      UserDao userDao = new UserDao();

      // login_idをゲットパラメータで取得
      String lId = request.getParameter("login_id");

      // 例外処理
      if (loginId.equals("") || password.equals("") || confirmPassword.equals("") || name.equals("")
          || birthday.equals("") || !password.equals(confirmPassword)
          || !userDao.oId(lId)) {

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        userDao.insert(loginId, password, name, birthday);

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");

        // エラー時、ユーザーの入力した値を表示
        request.setAttribute("inputloginId", loginId);

        // 入力された値を画面に表示するためリクエストスコープに値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthday", birthday);

        // ユーザーに再度入力させるためuserAdd.jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
	}
}
