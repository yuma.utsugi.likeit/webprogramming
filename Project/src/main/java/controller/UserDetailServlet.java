package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserDetailServlet */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserDetailServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // idをゲットパラメータで取得
    String id = request.getParameter("id");

    // int型に変換
    int id2 = Integer.valueOf(id);

    // 上記の値をもとにuserDaoのfindByIdメソッドを呼び出す
    UserDao userDao = new UserDao();
    User user = userDao.findById(id2);

    // メソッドの戻り値をリクエストスコープにセット
    request.setAttribute("user", user);

    // フォワード文でuserDetail.jspに遷移
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
    dispatcher.forward(request, response);
    return;
  }
}
